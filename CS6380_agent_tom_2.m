function action = CS6380_agent_tom_2(percept)
% CS6380_agent_tom_2 - walk into a wall agent
% On input:
%     percept (struct vector): agent percept data
%       .gold (Boolean): gold nearby
%       .gold_dist (float): distance to gold
%       .gold_heading (float): angle to gold (radians)
%       .x (float): x position of agent
%       .y (float): y position of agent
%       .h (float): angle of agent heading
%       .range (1x360 vector): range sensor values (per 1 degree)
%       .pheromone (2x1 vector): pheromone gradient ([0;0] if none
%       .bump (Boolean): ran into wall
%       .time (float): current time
%       .del_t (float): time step
% On output:
%     action (struct vector): agent actions
%       .heading (float): direction to move (this happens first)
%       .speed (float): speed to move
%       .grab (Boolean): grab gold
%       .drop (Boolean): drop gold
%       .pheromone (Boolean): drop pheromone at current location
% Call:
%     action = CS6380_agent_tom_2(percept);
% Author:
%     T. Henderson
%     UU
%     Spring 2020
%

if percept.bump==1
    display('Agent Tom 2 Bump');
end
action.heading = 0;
action.speed = 10;
action.grab = 0;
action.drop = 0;
action.pheromone = 0;
