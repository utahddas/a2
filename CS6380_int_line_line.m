function [intersection,status] = CS6380_int_line_line(line1,line2)
%
% On input:
%     line1: 2 points defining line (matrix 2x3)
%     line2: 2 points defining line (matrix 2x3)
% On output:
%     intersection: 2 points  if intersect in line (matrix 2x3)
%                   inf       if lines don't intersect
%                   3D point  if lines intersect
%     status: -1   if lines are same line
%              0   if lines do not intersect
%              1   if lines intersect in a single point
% Author:
%     T. Henderson
%     UU
%     Spring 2020
%

SAME_THRESH = 0.001;
PARA_THRESH = 0.99;

intersection = [];

[a1,b1,c1] = CS6380_line_fit2D(line1(1,1:2)',line1(2,1:2)');
[a2,b2,c2] = CS6380_line_fit2D(line2(1,1:2)',line2(2,1:2)');

if norm([a1,b1,c1]-[a2,b2,c2])<SAME_THRESH
    status = -1;
    intersection = line1;
    return
end

if abs(dot([a1,b1],[a2,b2]))>PARA_THRESH
    status = 0;
    intersection = inf;
    return
end

A = [a1,b1; a2,b2];
b = [-c1;-c2];
status = 1;
intersection = [(A\b)',0];
