function CS6380_show_my_range(x,y,range)
% CS6380_show_my_range - plots range data in environment
% On input:
%     x (float): x location of sensor
%     y (float): y location of sensor
%     range (float): range sensor data
% On output:
%     N/A
% Call:
%     CS6380_show_my_range(xa,ya,range);
% Author:
%     T. Henderson
%     UU
%     Spring 2020
%

hold on

n = length(range);
thetad = [0:360/n:360];
thetad = thetad(1:n);
theta = thetad*pi/180;
for d = 1:n
    if range(d)>=0
        plot([x,x+range(d)*cos(theta(d))],[y,y+range(d)*sin(theta(d))],'r');
    end
    tch = 0;
end
