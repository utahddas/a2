function action = CS6380_agent_david_rand(percept)
% CS6380_agent_tom_2 - walk into a wall agent
% On input:
%     percept (struct vector): agent percept data
%       .gold (Boolean): gold nearby
%       .gold_dist (float): distance to gold
%       .gold_heading (float): angle to gold (radians)
%       .x (float): x position of agent
%       .y (float): y position of agent
%       .h (float): angle of agent heading
%       .range (1x360 vector): range sensor values (per 1 degree)
%       .pheromone (2x1 vector): pheromone gradient ([0;0] if none
%       .bump (Boolean): ran into wall
%       .time (float): current time
%       .del_t (float): time step
% On output:
%     action (struct vector): agent actions
%       .heading (float): direction to move (this happens first)
%       .speed (float): speed to move
%       .grab (Boolean): grab gold
%       .drop (Boolean): drop gold
%       .pheromone (Boolean): drop pheromone at current location
% Call:
%     action = CS6380_agent_tom_2(percept);
% Author:
%     D. Sacharny
%     UU
%     Spring 2020
%
    persistent prev_action;
    update_times = .1:5:10;
    if isempty(prev_action)
        action.heading = pi/3;
        action.speed = 10;
        action.grab = 0;
        action.drop = 0;
        action.pheromone = 0;
        prev_action = action;
    end
    action = prev_action;
    
    
    if any(update_times - percept.time < .1)
        disp('Updating');
        r = 10*percept.del_t*(pi/10);
        action.heading = prev_action.heading + 2*r*rand() - r;
        %action.speed = prev_action.speed + randn();
    end
    
    if percept.bump
        disp('BUMP!');
        action.heading = prev_action.heading + pi/2;
    end
    
    prev_action = action;
end

