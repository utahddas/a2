function grad = CS6380_pheromone_gradient(x,y,pheromones,range)
% CS6380_nearest_gold - return index of nearest gold piece in range
% On input:
%     x (float): x coordinate of agent
%     y (float): y coordinate of agent
%     pheromones (nx5 array): list of pheromone information
%       col 1: x location
%       col 2: y location
%       col 3: range of influence
%       col 4: strength of pheromone
%       col 5: time deposited
%     range (float): detection radius range
% On output:
%     grad (2x1 vector): direction of pheromone gradient in vicinity
% Call:
%     grad = CS6380_pheromon_gradient([xa;ya],pheromones,PHEROMONE_DIST);
% Author:
%     T. Henderson
%     UU
%     Spring 2020
%

grad = [0;0];

if isempty(pheromones)
    return
end
num_pheromones = length(pheromones(:,1));

info = [];
for p = 1:num_pheromones
    d = norm([pheromones(p,1);pheromones(p,2)]-[x;y]);
    if d<=range
        info = [info; p,d,pheromones(p,4)];
    end
end
if ~isempty(info)
    if length(info(:,1))==1
        return
    end
    index_min = find(info(:,3)==min(info(:,3)));
    index_min = index_min(1);
    index_max = find(info(:,3)==max(info(:,3)));
    index_max = index_max(1);
    dpdx = pheromones(info(index_max,1),1)...
        -pheromones(info(index_min,1),1);
    dpdy = pheromones(info(index_max,1),2)...
        -pheromones(info(index_min,1),2);
    grad = [dpdx;dpdy];
    if norm(grad)>0
        grad = grad/norm(grad);
    end
end
