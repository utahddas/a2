function index = CS6380_nearest_gold(pt,gold,range)
% CS6380_nearest_gold - return index of nearest gold piece in range
% On input:
%     pt (2x1 vector): location of agent (sensor)
%     gold (nx2 array): list of gold locations
%     range (float): sensor radius range
% On output:
%     index (int): index of nearest gold piece within range
% Call:
%     ind = CS6380_nearest_gold([xa;ya],gold,GOLD_DIST);
% Author:
%     T. Henderson
%     UU
%     Spring 2020
%

index = 0;

num_gold = length(gold(:,1));
min_dist = Inf;
for g = 1:num_gold
    g_loc = [gold(g,1);gold(g,2)];
    d = norm(pt-g_loc);
    if d<range&d<min_dist
        min_dist = d;
        index = g;
    end
end
