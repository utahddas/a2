function action = CS6380_agent_tom_4(percept)
% CS6380_agent_tom_3 - go to [40;0]
% On input:
%     percept (struct vector): agent percept data
%       .gold (Boolean): gold nearby
%       .gold_dist (float): distance to gold
%       .gold_heading (float): angle to gold (radians)
%       .x (float): x position of agent
%       .y (float): y position of agent
%       .h (float): angle of agent heading
%       .range (1x360 vector): range sensor values (per 1 degree)
%       .pheromone (2x1 vector): pheromone gradient ([0;0] if none
%       .bump (Boolean): ran into wall
%       .time (float): current time
%       .del_t (float): time step
% On output:
%     action (struct vector): agent actions
%       .heading (float): direction to move (this happens first)
%       .speed (float): speed to move
%       .grab (Boolean): grab gold
%       .drop (Boolean): drop gold
%       .pheromone (Boolean): drop pheromone at current location
% Call:
%     action = CS6380_agent_tom_4(percept);
% Author:
%     T. Henderson
%     UU
%     Spring 2020
%

persistent goal

if isempty(goal)
    goal = [40;0];
end

xa = percept.x;
ya = percept.y;
ha = percept.h;
range = percept.range;

if percept.bump==0  % move forward
    new_heading = goal - [xa;ya];
    new_heading = new_heading/norm(new_heading);
    theta = posori(atan2(new_heading(2),new_heading(1)));
    action.heading = theta;
    action.speed = 10;
    action.grab = 0;
    action.drop = 0;
    action.pheromone = 0;
else % rotate left along line segment
    num_range_sensors = length(range);
    theta1 = ha;
    index1 = floor(theta1) + 1;
    if index1==num_range_sensors
        index2 = 1;
    else
        index2 = index1 + 1;
    end
    theta2 = index2*pi/180;
    pt1 = [xa;ya] + [cos(theta1);sin(theta1)]*range(index1);
    pt2 = [xa;ya] + [cos(theta2);sin(theta2)]*range(index2);
    [a,b,c] = CS6380_line_fit2D(pt1,pt2);
    pts = [];
    for r = 1:num_range_sensors
        pt = [xa;ya] + [cos((r-1)*pi/180);sin((r-1)*pi/180)]*range(r);
        if abs(pt(1)*a+pt(2)*b+c)<0.001
            pts = [pts;pt'];
        end
    end
    [p1,s1] = CV_total_LS(pts(:,1),pts(:,2));
    if dot([cos(theta1);sin(theta1)],[p1(1),p1(2)])<0
        p1(1) = -p1(1);
        p1(2) = -p1(2);
    end
    line_dir = [-p1(2);p1(1)];
end
