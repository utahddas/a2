function range = CS6380_range_scan(x,y,segments,num_sensors,max_range)
% CS6380_drive_A2_ABMS - overall driver for A2 ABMS
% On input:
%     x (float): x location of sensor
%     y (float): y location of sensor
%     segments (nx4 array): line segments
%     num_sensors (int): number of range sensors (in circle)
%     max_range (float): maximum range of detection of surface
% On output:
%     res (struct vector): location and range results at each step
%       .x (float): x location of agent 1
%       .y (float): y location of agent 1
%       .range (1x360 vector) range data at [x;y]
% Call:
%     range = CS6380_range_scan(xa,ya,segments,NUM_SENSORS,MAX_RANGE);
% Author:
%     T. Henderson
%     UU
%     Spring 2020
%

LINE_THRESH = 0.0001;

range = -ones(1,num_sensors);
thetad = [0:360/num_sensors:360];
thetad = thetad(1:num_sensors);
theta = thetad*pi/180;
[num_segments,dummy] = size(segments);

for d = 1:num_sensors
    pt2 = [x+cos(theta(d));y+sin(theta(d))];
    ray = [x,y,0; pt2(1),pt2(2),0];
    ray_dir = [pt2(1)-x;pt2(2)-y];
    min_d = Inf;
    for s = 1:num_segments
        line_s = [segments(s,1),segments(s,2),0;...
            segments(s,3),segments(s,4),0];
%        pt = CS6380_int_line_line(ray,line_s);
        pt = cv_int_line_line(ray,line_s,LINE_THRESH);
        if ~isinf(pt)
            % check line intersection in ray direction
            pt_dir = pt(1:2)' - [x;y];
            pt_dist = norm(pt_dir);
            if pt_dist==0
                OK = 0;
            else
                pt_dir = pt_dir/norm(pt_dir);
                OK = dot(pt_dir,ray_dir)>0.9;
            end
            % check intersection on segment
            s1 = line_s(1,1:2)';
            s2 = line_s(2,1:2)';
            in_seg = norm(pt(1:2)'-s1) + norm(pt(1:2)'-s2) <= norm(s1-s2);
            r = norm([x;y]-[pt(1);pt(2)]);
            if in_seg&OK==1&r<min_d&r<max_range
                min_d = r;
            end
        end
    end
    if ~isinf(min_d)
        range(d) = min_d;
    end
end
tch = 0;
