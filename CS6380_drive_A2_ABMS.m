function [res,M,segments,gold] = CS6380_drive_A2_ABMS(max_t,num_gold)
% CS6380_drive_A2_ABMS - overall driver for A2 ABMS
% On input:
%     N/A
% On output:
%     res (struct vector): location and range results at each step
%       .x (float): x location of agent 1
%       .y (float): y location of agent 1
%       .range (1x360 vector) range data at [x;y]
% Call:
%     [r1,M1,s1,g1] = CS6380_drive_A2_ABMS;
% Author:
%     T. Henderson
%     UU
%     Spring 2020
%

res = [];
M = [];

X_MIN = -50;
X_MAX = 50;
Y_MIN = -50;
Y_MAX = 50;

num_gold = num_gold;
del_t = 0.1;
fnames(1).name = 'CS6380_agent_tom_5';
%fnames(2).name = 'CS6380_agent_tom_8';
%fnames(3).name = 'CS6380_agent_tom_9';
%fnames(4).name = 'CS6380_agent_tom_10';
num_agents = length(fnames);
for a = 1:num_agents
    clear(fnames(a).name);
end

segments = [...
    -50,-50,50,-50;...
    50,-50,50,50;...
    50,50,-50,50;...
    -50,50,-50,-50;...  % outer boundaries
    -20,-30,-10,-30;...
    -10,-30,-10,-20;...
    -10,-20,-20,-20;...
    -20,-20,-20,-30;... %rectangle 1
    25,-30,30,-30;
    30,-30,30,10;
    30,10,25,10;
    25,10,25,-30;...  % rectangle 2
    -40,20,-20,20;
    -20,20,-20,40;
    -20,40,-40,40;
    -40,40,-40,20];  % rectangle 3

gold = [X_MIN+rand(num_gold,1)*(X_MAX-X_MIN),...
    Y_MIN+rand(num_gold,1)*(Y_MAX-Y_MIN)];

res = CS6380_A2_ABMS(fnames,segments,max_t,del_t,gold,1);

%M = CS6380_show_trace(res,segments);

tch = 0;
