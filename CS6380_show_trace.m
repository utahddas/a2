function M = CS6380_show_trace(res,segments)
% CS6380_show_my_range - plots range data in environment
% On input:
%     res (struct vector): trace of agents
%       .agents (7nx1 vector): all agent info
%       .maps (1001x1001x6 array): set of maps
%     segments (nx4 array): segment dat
% On output:
%     M (movie): movie of agent trajectories
% Call:
%     CS6380_show_trace(res,segments);
% Author:
%     T. Henderson
%     UU
%     Spring 2020
%

num_steps = length(res);
num_agents = length(res(1).agents)/7;
x_min = min([segments(:,1),segments(:,3)]);
x_max = max([segments(:,1),segments(:,3)]);
y_min = min([segments(:,2),segments(:,4)]);
y_max = max([segments(:,2),segments(:,4)]);
for s = 1:num_steps
    figure(1);
    clf
    CS6380_draw_segments(segments);
    agents = reshape(res(s).agents,num_agents,7);
    plot(x_max+20,0,'w.');
    gold = res(s).gold;
    plot(gold(:,1),gold(:,2),'y*');
    plot(gold(:,1),gold(:,2),'k.');
    for a = 1:num_agents
        text(agents(a,1),agents(a,2),num2str(a),'Color','blue',...
            'FontWeight','bold');
    end
    text(51,45,['Step: ',num2str(s)]);
    drawnow;
    M(s) = getframe(gca);
end

if isempty(res(1).maps)
    return
end

merged_maps = zeros(1001,1001);
for f = 1:num_agents
    figure(f+1);
    merged_maps = merged_maps>0 | res(1).maps(:,:,f)>0;
    imshow(res(1).maps(:,:,f));
    title(['Agent ',num2str(f)]);
end
figure(num_agents+1);
imshow(merged_maps);
title('Merged Maps');
