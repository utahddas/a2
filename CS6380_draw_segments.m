function CS6380_draw_segments(segments)
% CS6380_drive_A2_ABMS - overall driver for A2 ABMS
% On input:
%     segments (nx4 array): line segments (each row is two endpoints)
%       4 rows give: lower, right, upper, left segments
% On output:
%     N/A
% Call:
%     CS6380_draw_segments(segments);
% Author:
%     T. Henderson
%     UU
%     Spring 2020
%

clf
[num_segments,dummy] = size(segments);
plot(0,0,'w.');
hold on
axis equal
x_min = min([segments(:,1);segments(:,3)]);
x_max = max([segments(:,1);segments(:,3)]);
y_min = min([segments(:,2);segments(:,4)]);
y_max = max([segments(:,2);segments(:,4)]);
plot(x_min-1,y_min-1,'w.');
plot(x_max+1,y_max+1,'w.');

if num_segments==0
    return
end

for s = 1:num_segments
    plot([segments(s,1),segments(s,3)],[segments(s,2),segments(s,4)],'k');
end
