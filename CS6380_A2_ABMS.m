function res = CS6380_A2_ABMS(fnames,segments,max_t,del_t,gold,draw)
% CS6380_A2_ABMS - A2 ABMS simulator
% On input:
%     fnames (struct vector): names of agent function (filenames)
%     segments (nx4 array): n line segments (lower left, lower right,...
%         upper right, upper left corners)
%     max_t (float): max time to simulate
%     del_t (float): time step increment
%     gold (px2 array): locations of gold
%     draw (Boolean): display each simulation step
% On output:
%     res (struct vector): agent info at each step
%       .agents (n*7 x 1 vector): linearized nx7 table of agent info
%       .gold (kx2 array): gold locations at this step
% Call:
%     r1 = CS6380_A2_ABMS(fnames,segments,max_t,del_t,gold,0);
% Author:
%     T. Henderson
%     UU
%     Spring 2020
%

X_MIN = -50;
X_MAX = 50;
Y_MIN = -50;
Y_MAX = 50;
NUM_RANGE_SENSORS = 360;
GOLD_DIST = 2;
BUMP_DIST = 0.1;
PHEROMONE_DIST = 5;
MIN_PHEROMONE = 0.1;
MAX_PHEROMONE = 5;
LAMBDA = 1;
MAX_RANGE = 30;
MAX_SPEED = 10;
WALL_EPS = 0.1;
ORIGIN_EPS = 0.5;

[num_segments,dummy] = size(segments);
num_agents = length(fnames);
agents = zeros(num_agents,7);
%  1 2 3        4         5          6            7
%  x y h desired_speed have_gold total_gold actual_speed
pheromones = [];
if isempty(gold)
    num_gold = 0;
else
    num_gold = length(gold(:,1));
end
cur_time = 0;
maps = zeros(1001,1001,num_agents);

count = 0;
wb = waitbar(0,'Run ABMS A2');
while cur_time<max_t
    cur_time = cur_time + del_t;
    waitbar(cur_time/max_t);
    for a = 1:num_agents
        xa = agents(a,1);
        ya = agents(a,2);
        ha = agents(a,3);
        % initialize default percept values
        percept.gold = 0;
        percept.gold_dist = 0;
        percept.gold_heading = 0;
        percept.x = xa;
        percept.y = ya;
        percept.h = ha;
        percept.range = -ones(1,NUM_RANGE_SENSORS);
        percept.pheromone = zeros(2,1);
        percept.bump = 0;
        percept.time = cur_time;
        percept.del_t = del_t;
        % gold percept
        g_index = CS6380_nearest_gold([xa;ya],gold,GOLD_DIST);
        if g_index>0
            percept.gold = 1;
            a_loc = [xa;ya];
            g_loc = [gold(g_index,1);gold(g_index,2)];
            percept.gold_dist = norm(a_loc-g_loc);
            percept.gold_heading = posori(atan2(g_loc(2)-a_loc(2),...
                g_loc(1)-a_loc(1)));
        end
        % pheromone percept
        grad = CS6380_pheromone_gradient(xa,ya,pheromones,PHEROMONE_DIST);
        percept.pheromone = grad;
        % range
        range = CS6380_range_scan(xa,ya,segments,NUM_RANGE_SENSORS,...
            MAX_RANGE);
        percept.range = range;
        % bump percept
        r_index = floor(ha*180/pi) + 1;
        dir = [cos(ha);sin(ha)];
        speed = agents(a,4);
        pt = [xa;ya] + dir*speed*del_t;
        dist = norm(pt-[xa;ya]);
        if range(r_index)>0&dist>=range(r_index)-WALL_EPS
            percept.bump = 1;
        end
        % Call agent
        action = feval(fnames(a).name,percept);
        % Update world
        actions(a) = action;
        percepts(a) = percept;
    end
    % update agent state (execute actions)
    if ~isempty(pheromones)
        pheromones(:,4) = ...
            MAX_PHEROMONE*exp(-LAMBDA*(cur_time-pheromones(:,5)));
        indexes = find(pheromones(:,4)<MIN_PHEROMONE);
        pheromones(indexes,:) = [];
    end
    for a = 1:num_agents
        % update gold
        g_index = CS6380_nearest_gold(agents(a,1:2)',gold,GOLD_DIST);
        if actions(a).grab==1&g_index>0&agents(a,5)==0
            agents(a,5) = 1;
            gold(g_index,:) = [];
        end
        if agents(a,5)==1&actions(a).drop==1
            agents(a,5) = 0;
            if norm(agents(a,1:2)'-[0;0])<ORIGIN_EPS
                agents(a,6) = agents(a,6) + 1;
            else
                gold = [gold; agents(a,1:2)];
            end
        end
        % update pheromone
        if actions(a).pheromone==1
            pheromones = [pheromones;agents(a,1:2),PHEROMONE_DIST,...
                MAX_PHEROMONE,cur_time];
        end
        % update heading
        ha = posori(actions(a).heading);
        agents(a,3) = ha;
        a_dir = [cos(ha);sin(ha)];
        % update speed
        speed = max(min(MAX_SPEED,actions(a).speed),0);
        agents(a,4) = speed;
        % update position
        xa = agents(a,1);
        ya = agents(a,2);
        p = agents(a,1:2)' + speed*a_dir*del_t;
        dist = norm(p-[xa;ya]);
        percept = percepts(a);
        range = percept.range;
        index = floor(ha*180/pi) + 1;
        if range(index)<0
            dist_moved = dist;
        else
            dist_moved = min(dist,range(index)-WALL_EPS);
            if dist>dist_moved
                agents(a,7) = 0;
            end
        end
        agents(a,1) = xa + dist_moved*cos(ha);
        agents(a,2) = ya + dist_moved*sin(ha);
        tch = 0;
        if isempty(actions(a).map)
            maps = [];
        else
            maps(:,:,a) = actions(a).map;
        end
    end
    count = count + 1;
    res(count).agents = agents(:);
    res(count).gold = gold;
    % step by step display of agents
    if draw==1
        figure(1);
        clf
        CS6380_draw_segments(segments);
        %    agents = reshape(res(s).agents,num_agents,7);
        plot(50+20,0,'w.');
        gold = gold;
        plot(gold(:,1),gold(:,2),'y*');
        plot(gold(:,1),gold(:,2),'k.');
        for a = 1:num_agents
            text(agents(a,1),agents(a,2),num2str(a),'Color','blue',...
                'FontWeight','bold');
        end
        text(51,45,['Step: ',num2str(count)]);
        drawnow;
    end
    % end of display
end
close(wb);
res(1).maps = maps;
